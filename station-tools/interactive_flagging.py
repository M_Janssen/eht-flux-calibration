#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
 - use plotmask() to iteratively mask points by drawing polygons
"""

import matplotlib
#matplotlib.use('Qt4Agg', warn=False, force=True)
import matplotlib.pyplot as plt
import shapely.geometry as sh_geom
import shapely.geometry.polygon as sh_polyg
import numpy as np


def if_inside_polygon(point_x, point_y, polygon_points, dummy_sigma=''):
    point          = (point_x, point_y)
    _point         = sh_geom.Point(point)
    _polygon       = sh_polyg.Polygon(polygon_points)
    return _polygon.contains(_point)


def _tellme(s):
    print(s)
    plt.title(s)
    plt.draw()


def _draw_mask(x,y, yerr=''):
    plt.clf()
    plt.setp(plt.gca(), autoscale_on=True)
    if any(yerr):
        plt.errorbar(x, y, yerr=yerr, fmt='o')
    else:
        plt.scatter(x,y)
    mng = plt.get_current_fig_manager()
    try:
        mng.resize(*mng.window.maxsize())
    except AttributeError:
        pass
    happy = False
    while not happy:
        pts = []
        _tellme('Select the corners of a polygon with left mouse clicks or by pressing any key but Backspace or Enter.\n'
                'Right mouse click or press Backspace to remove the latest corner drawn.\n'
                'To continue: Middle mouse click or press Enter when all corners are drawn. Points inside the polygon will be masked.'
               )
        pts = np.asarray(plt.ginput(-1, timeout=-1))
        if pts.any():
            ph = plt.fill(pts[:, 0], pts[:, 1], 'r', lw=2)
            _tellme('Happy? Key click for yes, mouse click to start over again.')
            happy = plt.waitforbuttonpress()
        else:
            happy = True
        # Get rid of fill
        if not happy:
            for p in ph:
                p.remove()
    plt.close()
    return pts


def _maskit(x,y, yerr=''):
    mask_area = _draw_mask(x,y, yerr)
    if mask_area.any():
        mask      = []
        for i,xval in enumerate(x):
            yval = y[i]
            mask.append(if_inside_polygon(xval,yval, mask_area))
        _x    = np.ma.masked_array(x, mask)
        _y    = np.ma.masked_array(y, mask)
        if any(yerr):
            _yerr = np.ma.masked_array(yerr, mask)
        else:
            _yerr = yerr
    else:
        _x,_y,_yerr = x,y,yerr
    return _x,_y, _yerr


def plotmask(x,y, yerr=''):
    """
     - Make plots of (x,y[,yerr]) data.
     - User can draw a polygon in the plot with left mouse clicks.
     - Can be done iteratively.
     - Returns (x*,y*[,yerr*]) from (x,y[,yerr]) where all points inside the polygons are masked (flagged).
    """
    gotall = False
    while not gotall:
        x,y, yerr = _maskit(x,y, yerr)
        plt.clf()
        plt.setp(plt.gca(), autoscale_on=True)
        mng = plt.get_current_fig_manager()
        try:
            mng.resize(*mng.window.maxsize())
        except AttributeError:
            pass
        if any(yerr):
            plt.errorbar(x, y, yerr=yerr, fmt='o')
        else:
            plt.scatter(x,y)
        _tellme('Key click to exit the program with the current masks applied.\nMouse click to draw another polygon for masking.')
        gotall = plt.waitforbuttonpress()
        plt.close()
    return x,y, yerr
