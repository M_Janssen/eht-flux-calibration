#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Parses APEX field system system temperature data and adds a duplicate entry with a time offset.
"""
from glob import glob


def alter_line(filename, search_pattern, new_line, str_char = '#'):
    f     = open(filename, 'r')
    lines = f.readlines()
    for i, l in enumerate(lines):
        if l[0] != str_char:
            if search_pattern in l:
                lines[i] = new_line
                break
    f.close()
    f = open(filename, 'w')
    for l in lines:
        f.write(l)
    f.close()


def find_between( string, first, last  ):
    try:
        start = string.index( first  ) + len( first  )
        end = string.index( last, start  )
        return string[start:end]
    except ValueError:
        return ""

def convert_timestring(instr, dd0, UTdd0):
    #from yy-mm-dd hh:mm:ss ts
    instr   = instr.split()
    instr_d = instr[0]
    instr_t = instr[1]
    instr_d = instr_d.split('-')
    utdd    = int(instr_d[2]) - dd0 + UTdd0
    return str(utdd)+' '+ instr_t


#must organize the fieldsys files into orig_*.AN files per track and add GAIN and IDENX lines
data_path = '/home/michael/data_collections/VLBI_data/EHT/2017_apr/Metadata/APEX/tsys_from_logs/'
infile    = 'orig_*.AN'
infiles   = glob(data_path+infile)
defaultsb = ["'R1:32'", "'L1:32'"]
othersb   = ["'R1:32'", "'L1:32'"]

baseday    = 04 #april 04 2017
baseday_UT = 94

tsys_xcp = [[],[]]

for f in infiles:
    ff  = f.replace('orig_', '')
    ff2 = ff.replace('sideband1', 'sideband2')
    xf  = open(ff, 'w')
    yf  = open(ff2, 'w')
    infile = open(f, 'r')
    lines  = infile.readlines()
    for i,line in enumerate(lines):
        if i < 3:
            xf.write(line)
            for d,o in zip(defaultsb,othersb):
                if d in line:
                    line = line.replace(d,o)
            yf.write(line)
            continue
        if 'vlbi_get_calibration' in line:
            time = line.split('@')
            time = convert_timestring(time[1].lstrip(), baseday, baseday_UT)
        if 'BE-group/F401' in line:
            xcp = 1
        if 'BE-group/F402' in line:
            xcp = 0
        if 'tsys' in line:
            tsys = line.split('/')[1].replace(',trx', '')
            tsys_xcp[xcp] = tsys
        if tsys_xcp[0]:
            if tsys_xcp[1]:
                xf.write(time + ' ' + tsys_xcp[0] + ' ' + tsys_xcp[1] + '\n')
                yf.write(time + ' ' + tsys_xcp[0] + ' ' + tsys_xcp[1] + '\n')
                tsys_xcp = [[],[]]
    infile.close()
    xf.write('/\n')
    yf.write('/\n')
    xf.close()
    yf.close()
    xf  = open(ff, 'r')
    yf  = open(ff2, 'r')
    xlines = xf.readlines()
    ylines = yf.readlines()
    xf.close()
    yf.close()
    xf  = open(ff, 'a')
    yf  = open(ff2, 'a')
    for xl,yl in zip(xlines,ylines):
        xf.write(xl)
        yf.write(yl)
    xf.close()
    yf.close()
    alter_line(ff, 'TSYS AP timeoff', 'TSYS AP timeoff= -120.0 FT = 1.0\n')
    alter_line(ff, 'TSYS AP timeoff= 1.0', 'TSYS AP timeoff= 120.0 FT = 1.0\n')
    alter_line(ff2, 'TSYS AP timeoff', 'TSYS AP timeoff= -120.0 FT = 1.0\n')
    alter_line(ff2, 'TSYS AP timeoff= 1.0', 'TSYS AP timeoff= 120.0 FT = 1.0\n')
