#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""Fit gain curve to data."""
import numpy as np
import matplotlib.pyplot as plt
import numpy.polynomial.polynomial as poly
from scipy.optimize import curve_fit
import interactive_flagging

def gc_func(el, A, el_max):
    return 1. - A*(el-el_max)**2

def gc_fit(elevations, Ta_star_vals, sigma, degree=2, renormalize_to_poly=True, plateaurange=[40,60]):
    """fit to  gc= 1. - A*(elevation-el_max)**2  to fix a0=1. A should be <1"""
    _elevations, _Ta_star_vals, _sigma = np.transpose([tmp for tmp in sorted(zip(elevations, Ta_star_vals, sigma))])
    _elevations, _Ta_star_vals, _sigma = interactive_flagging.plotmask(elevations, Ta_star_vals, sigma)
    x_fit     = np.linspace(np.ma.min(_elevations), np.ma.max(_elevations), 10*len(_elevations))
    pl_fluxes = []
    for elev,Ta_s in zip(_elevations, Ta_star_vals):
        if plateaurange[0] < elev < plateaurange[1]:
            pl_fluxes.append(Ta_s)
    avg_flux        = np.ma.mean(np.ma.asarray(pl_fluxes))
    norm_flux       = [ta/avg_flux for ta in _Ta_star_vals]
    norm_sigma      = [s/avg_flux for s in _sigma]
    coeffs_init, _  = np.ma.polyfit(_elevations, norm_flux , degree, w=norm_sigma, cov=True)
    coeffs_init     = coeffs_init[::-1]
    ffit_init       = poly.polyval(x_fit, coeffs_init)
    diff            = abs(1. - max(ffit_init))
    if renormalize_to_poly:
        avg_flux2 = avg_flux /(1. - diff)
    else:
        avg_flux2 = 1.
    norm_flux2      = [ta/avg_flux2 for ta in norm_flux]
    norm_sigma2     = [s/avg_flux2 for s in norm_sigma]
    elevations_noM  = []
    norm_flux2_noM  = []
    norm_sigma2_noM = []
    for vals in zip(_elevations, norm_flux2, norm_sigma2):
        if not any(np.isnan(vals)):
            elevations_noM.append(vals[0])
            norm_flux2_noM.append(vals[1])
            norm_sigma2_noM.append(vals[2])
    elevations_noM, norm_flux2_noM, norm_sigma2_noM = np.transpose([tmp for tmp in sorted(zip(elevations_noM, norm_flux2_noM,
                                                                                              norm_sigma2_noM
                                                                                             ))])
    _bounds                                         = ((-np.inf, min(plateaurange)), (np.inf, max(plateaurange)))
    try:
        popt, pcov = curve_fit(gc_func, elevations_noM, norm_flux2_noM, sigma=norm_sigma2_noM, bounds=_bounds)
    except TypeError:
        popt, pcov = curve_fit(gc_func, elevations_noM, norm_flux2_noM, sigma=norm_sigma2_noM)
    _A       = popt[0]
    _el_max  = popt[1]
    S_A      = np.sqrt(pcov[0])[0]
    S_el_max = np.sqrt(pcov[1])[1]
    s_a      = np.abs(S_A/_A) * 100
    s_el_max = np.abs(S_el_max/_el_max) * 100
    print ('\n---------------------------------------------------\n')
    print ("Fit parameter A: ", _A)
    print ("Relative error on parameter A: ", s_a, "%")
    print ("Fit peak elevation el_max: ", _el_max)
    print ("Relative error on peak elevation: ", s_el_max, "%")
    print ('\n---------------------------------------------------\n')
    fig,ax = plt.subplots(figsize=(28.0, 15.0))
    ax.set_xlabel('Elevation [degree]')
    ax.set_ylabel(r'Ta* [normalized]')
    ax.errorbar(_elevations, norm_flux2, yerr=norm_sigma, fmt='o')
    x_fit = np.linspace(min(elevations_noM), max(elevations_noM), 1000)
    plt.plot(x_fit, gc_func(x_fit, *popt))
    plt.grid()
    plt.show()
    return popt, s_a, s_el_max, elevations_noM, _elevations, norm_flux2, norm_sigma
