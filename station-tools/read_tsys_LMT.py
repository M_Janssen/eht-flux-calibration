#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""Reads short time cadence Tsys entries from LMT total power measurements."""
import datetime
import pandas as pd
import numpy as np


def frac_diff(x,y):
    _x = float(x)
    _y = float(y)
    return np.abs(_x-_y)/min(_x,_y)

def convert_timestring(instr, dd0, UTdd0):
    #from YYYY-mm-dd-hh-mm-ss-ff to UTdd hh:mm:ss
    instr = instr.split('-')
    utdd  = int(instr[2]) - dd0 + UTdd0
    hh    = instr[3]
    mm    = instr[4]
    ss    = instr[5]
    if int(instr[6]) > 500000:
        ss = str(int(ss)+1)
        if ss=='60':
            ss = '00'
            mm = str(int(mm)+1)
            if mm=='60':
                mm = '00'
                hh = str(int(hh)+1)
                if hh=='24':
                    utdd = utdd+1
    return str(utdd)+' '+hh+':'+mm+':'+ss

def convert_timestring2(instr, dd0, UTdd0):
    #from YYYY-mm-dd hh:mm:ss.ss to UTdd hh:mm:ss
    instr = instr.split(' ')
    time  = instr[1].split(':')
    date  = instr[0].split('-')
    utdd  = int(date[2]) - dd0 + UTdd0
    hh    = str(time[0])
    mm    = str(time[1])
    ss    = float(time[2])
    ss    = round(ss,0)
    if ss == 60:
        ss = str(int(ss))
        if ss=='60':
            ss = '00'
            mm = str(int(mm)+1)
            if mm=='60':
                mm = '00'
                hh = str(int(hh)+1)
                if hh=='24':
                    utdd = utdd+1
    else:
        ss = str(int(ss))
    return format(utdd, '03d')+' '+hh+':'+mm+':'+ss

baseday    = 04 #april 04 2017
baseday_UT = 94

data_path = '/home/michael/data_collections/VLBI_data/EHT/2017_apr/Metadata/LMT/'
infile    = 'eht_april2017_lmt_timedep_tsys_1savg_new.csv'
outfile   = 'LMT'
#for each track x{A,B,C,D,E,...} and sideband sb create LMT_track<x>_sideband<sb>.AN
sidebands   = ['1', '2']
indices     = ["'R1:32', 'L1:32'","'R1:32', 'L1:32'"]
tracks      = ['A', 'B', 'C', 'D', 'E']
track_times = [['099 23:17', '100 15:10'],
               ['096 00:46', '096 14:57'],
               ['097 06:25', '097 19:42'],
               ['094 22:31', '095 17:07'],
               ['100 22:16', '101 15:22']
              ]
#track_times = [['2017-04-09-23-17', '2017-04-10-15-10'],
#               ['2017-04-06-00-46', '2017-04-06-14-57'],
#               ['2017-04-07-06-25', '2017-04-07-19-42'],
#               ['2017-04-04-22-31', '2017-04-05-17-07'],
#               ['2017-04-10-22-16', '2017-04-11-15-22']
#              ]
opening  = 'TSYS LM INDEX = '
opening2 = '!T_sys_star'

df        = pd.read_csv(data_path+infile)
df_time   = df['Timestamp']
#df_time   = df['UTC']
#df_time   = [dft.replace(', ','-') for dft in df_time]
df_vlbi   = df['Obspgm']
df_tsys_R = df['Tsys_B']
df_tsys_L = df['Tsys_A']
sb_ratio  = 2.

all_files = []
for sb,index in zip(sidebands,indices):
    all_files1 = []
    for track in tracks:
        f = open(data_path+outfile+'_track'+track+'_sideband'+sb+'.AN','w')
        f.write(opening + index +' /\n'+opening2+'\n')
        all_files1.append(f)
    all_files.append(all_files1)

for time,tsysR,tsysL,vlbi in zip(df_time,df_tsys_R, df_tsys_L,df_vlbi):
    if True:
        if vlbi == 'VlbiSched':
            _time = str(datetime.datetime.utcfromtimestamp(time))
            _time = convert_timestring2(_time, baseday, baseday_UT)
            #_time = time
            for i,tt in enumerate(track_times):
                if tt[0] < _time < tt[1]:
                    #form_time = convert_timestring(_time, baseday, baseday_UT)
                    for sb_file in all_files:
                        if frac_diff(tsysR,tsysL) > 0.5:
                            print 'bad vals RCP:{0} and LCP:{1} at t={2}'.format(str(tsysR),str(tsysL), str(_time))
                        sb_file[i].write(_time+' '+str(sb_ratio * tsysR)+' '+str(sb_ratio * tsysL)+'\n')
                        #sb_file[i].write(form_time+' '+str(sb_ratio * tsysR)+' '+str(sb_ratio * tsysL)+'\n')

for f in all_files:
    for ff in f:
        ff.write('/')
        ff.close()
