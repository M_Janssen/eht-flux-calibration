#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Concatenates ANTAB files.
Can give multiple folders (space seprated) as command line argument for the parser.
"""
import os
import sys
from optparse import OptionParser


def main():
    usage       = "%prog folders  [-n <OUTFILE NAME>]  [-e <EXTENSION NAME>] [-s <DIR CUT>] -o  [-h or --help]"
    parser      = OptionParser(usage=usage)
    parser.add_option("-n", type="string",dest="outname",default="ALMA_concat.AN",
                      help=r"Name for the concatenated output file. [Default: %default]")
    parser.add_option("-e", type="string",dest="extension",default=".ANTAB",
                      help=r"Unique file extension of ANTAB tables in specified folders. [Default: %default]")
    parser.add_option("-s", type="string",dest="sort", default="0",
                      help=r"Directory level used to sort ANTAB. [Default: %default]")
    parser.add_option("-o", type="string",dest="overwrite",default="",
                      help=r"If True extrapolate to last IF if it is missing. [Default: %default]")
                      #first file in input list must have the full INDEX header
    (opts,args) = parser.parse_args()
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    concan(args, opts.extension, opts.outname, opts.sort, opts.overwrite)


def concan(_folders, _extension, _outf, _sort, _overwrite):
    """
    Concatenates ANTAB files.
    Can give multiple folders (space seprated) as command line argument for the parser.
    """
    matches     = []
    for folder in _folders:
        for root, dirs, files in os.walk(folder, followlinks=True):
            for filename in files:
                if filename.endswith(_extension):
                    matches.append(os.path.join(root, filename))
    #hopefully the sorting will also lead to sorted time entries for the concatenated file:
    sortlvl = int(_sort)
    if sortlvl:
        anfiles = sorted(matches, key = lambda matches: matches.split('/')[sortlvl])
    else:
        anfiles = sorted(matches)
    outf    = open(_outf, 'w')
    parsedf = [anfiles[0]]
    with open(anfiles[0], 'r') as infile:
        index  = ''
        indx0  = ''
        header = ''
        for line in infile:
            if 'GAIN' in line or 'POLY' in line:
                header += line
            elif 'TSYS' in line and 'INDEX' not in line:
                indx0  += line
            elif 'INDEX' in line:
                index  = line
    outf.write(header.strip('/'))
    outf.write('/\n')
    outf.write(indx0.strip('/'))
    outf.write(index.strip('/'))
    outf.write('/\n')
    for anf in anfiles:
        with open(anf, 'r') as infile:
            extrapolate = False
            for line in infile:
                if 'INDEX' in line:
                    if line != index:
                        if _overwrite:
                            extrapolate = True
                        else:
                            raise IOError('Index of ' + anf + ':\n ' + line + '\ndoes not match with the rest of the files\n\n' + \
                                          str(parsedf) + ':\n ' + index)
                    else:
                        parsedf.append(anf)
                elif ':' in line:
                    if extrapolate and '/' not in line:
                        values = line.strip('\n').split()
                        values.append(values[-1])
                        outf.write('  '.join(values)+'\n')
                    else:
                        outf.write(line.strip('/'))
    outf.write('/\n')
    outf.close()


if __name__=="__main__":
    main()

