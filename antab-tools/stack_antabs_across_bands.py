#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Combines EHT ANTAB tables from multiples bands into one single file.
- Usage: python stack_antabs_across_bands.py <year> <table1> <table2> ...
- Divides the Tsys measurements by the DPFU per band.
- Writes DPFU=1.0 for all stations as a new header based on the table given as first command line argument.
- Combines measurements with timeoff parameters.
- Replaces NaN values and missing values per band with zero.
- Writes Tsys with increasing index values in the order as given as command line arguments.
"""
import sys
import math
import time
import numpy as np
from collections import defaultdict

#example usage: stack_antabs_across_bands.py 2018 e18c21.AN e18c21_b1_proc.AN e18c21_b2_proc.AN e18c21_b3_proc.AN e18c21_b4_proc.AN
YEAR = int(sys.argv[1])
new_table = sys.argv[2]
to_be_stacked = sys.argv[3:]

def get_vals(antab):
    with open(antab, 'r', encoding='utf-8') as f:
        header = {}
        st_ind = {}
        t_vals = defaultdict(dict)
        for l in f:
            tsys = True
            line = l.split()
            #print(line)
            if not line or l[0]=='!' or l[0]=='/' or line[0]=='!' or line[0]=='/' or 'POLY' in line[0]:
                continue
            if 'GAIN' in line or 'POLY' in line:
                header = header | proc_header(line) # append dict
                tsys   = False
                continue
            elif 'TSYS' in line:
                _st, toff = proc_start0(line)
                tsys      = False
                if 'INDEX' not in line:
                    continue
            if 'INDEX' in l:
                _indxs = l.replace('=','').replace('/','').replace(',',' ').replace("'","").split('INDEX')[1].split()
                dpfu   = np.array([header[_st]['dr'] if 'R' in x else header[_st]['dl'] for x in _indxs])
                tsys   = False
            st_ind[_st] = _indxs
            if tsys:
                secs,vals = proc_tsys(line, toff, dpfu)
                t_vals[_st][secs] = vals
    return header, st_ind, t_vals

def proc_header(header_line):
    #GAIN <STATION> ELEV DPFU = <v1>[, <v2>] POLY = ...
    st = header_line[1]
    dr = float(header_line[5].replace(',',''))
    gc = 'POLY = 1.0 /'
    try:
        dl = float(header_line[6])
        if 'POLY' in header_line:
            gc = ' '.join(header_line[7:])
    except ValueError:
        dl = dr
        if 'POLY' in header_line:
            gc = ' '.join(header_line[6:])
    return {st:{'dr':dr, 'dl':dl, 'gc':gc}}


def proc_start0(indx0_line):
    #TSYS <STATION> timeoff= <toff> FT = ...
    st   = indx0_line[1]
    toff = 0
    for i,x in enumerate(indx0_line):
        if 'timeoff=' in x.lower():
            try:
                toff = float(x.lower().replace('timeoff=', ''))
            except ValueError:
                toff = float(indx0_line[i+1])
    if toff<1.1:
        toff = 0
    return st, toff


def proc_tsys(fields, timeoff, dpfu):
    # based on JIVE scripts
    tm_year = YEAR
    tm_yday = int(fields[0])
    _time   = fields[1]
    if _time.count(':') == 2:
        _time   = _time.split(':')
        tm_hour = int(round(float(_time[0])))
        tm_min  = int(round(float(_time[1])))
        tm_sec  = int(round(float(_time[2])))
    else:
        tm_hour = int(round(float(_time.split(':')[0])))
        tm_min  = math.modf(float(fields[1].split(':')[1]))
        tm_sec  = int(round(float(60 * tm_min[0])))
        tm_min  = int(round(float(tm_min[1])))
    if tm_sec == 60:
        tm_sec = 0
        tm_min += 1
    if tm_min == 60:
        tm_min  = 0
        tm_hour += 1
    if tm_hour == 24:
        tm_hour = 0
        tm_yday += 1
    if tm_yday == 365:
        tm_yday = 1
        tm_year += 1
    t = "%dy%03dd%02dh%02dm%02ds" % \
        (tm_year, tm_yday, tm_hour, tm_min, tm_sec)
    t = time.strptime(t, "%Yy%jd%Hh%Mm%Ss")
    secs = time.mktime(t) + timeoff
    vals = np.array([float(x) for x in fields[2:]]) / dpfu
    vals[np.isnan(vals)] = 0
    vals = [str(x) for x in vals]
    return secs, vals


def secs_to_antabtimefmt(secs):
    # from secs as returned by proc_tsys()
    return time.strftime('%j %H:%M:%S', time.localtime(secs))


indices = {}
tsys_entries = {}
all_times = {}
longest_header = {}
for tbs in to_be_stacked[::-1]:
    x,y,z = get_vals(tbs)
    if len(x) > len(longest_header):
        longest_header = x
    indices[tbs] = y
    tsys_entries[tbs] = z
    for station in z:
        try:
            all_times[station].extend(list(z[station].keys()))
        except KeyError:
            all_times[station] = list(z[station].keys())
for station in all_times:
    all_times[station] = sorted(np.unique(all_times[station]))

with open(new_table, 'w', encoding='utf-8') as f:
    for station in longest_header:
        f.write(f"GAIN {station} ELEV DPFU = 1.0 {longest_header[station]['gc']}\n")
    f.write("\n\n")

    for station in all_times:
        final_indx = "INDEX = "
        offset = 0
        for tbs in to_be_stacked:
            try:
                this_index = indices[tbs][station]
            except KeyError:
                # Station not having all bands, assume some default INDEX..
                this_index = ['R1:32', 'L1:32']
                print(f'Assuming {this_index} for {station} for {tbs}')
            if '|' in this_index[0]:
                numbers = [int(x.replace('L','').replace('R','').split('|')[0])+offset for x in this_index]
                offset  = numbers[-1]
                numbers = [str(x) for x in numbers]
                final_indx += ", ".join([f"'L{x}|R{x}'" for x in numbers])
                final_indx += ", "
            else:
                # X:Y format separate for R and L
                numbers = this_index[0].replace('L', '').replace('R', '').split(':')
                numbers = [int(x)+offset for x in numbers]
                offset  = max(numbers)
                for ti in this_index:
                    final_indx += "'{0}{1}:{2}', ".format(str(ti[0]), str(numbers[0]), str(numbers[1]))
        final_indx = final_indx[:-2]
        f.write(f"TSYS {station} FT=1.0 TIMEOFF=0 {final_indx} /\n")

        for t in all_times[station]:
            f.write(secs_to_antabtimefmt(t) + " ")
            these_tsys = ''
            ncount     = 1
            for tbs in to_be_stacked:
                try:
                    these_tsys = tsys_entries[tbs][station][t]
                    for n in range(ncount-1):
                        # fill in missing values from previous band(s)
                        f.write(' '.join(['0']*len(these_tsys))  + ' ')
                    f.write(' '.join(these_tsys)  + ' ')
                    ncount = 1
                except KeyError:
                    if these_tsys:
                        # write values from previous band
                        f.write(' '.join(['0']*len(these_tsys))  + ' ')
                    else:
                        ncount+=1
            f.write("\n")

        f.write("/\n\n")
