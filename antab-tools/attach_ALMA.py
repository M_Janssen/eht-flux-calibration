#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Adds Tsys block from infile2 to the end of infile1 and write the result to newfile.
"""
import sys
from optparse import OptionParser

def main():
    usage       = "%prog infile1 infile2 newfile [-h or --help]"
    parser      = OptionParser(usage=usage)
    (opts,args) = parser.parse_args()
    if len(sys.argv)<4:
        parser.print_help()
        sys.exit(1)
    addan(args[0], args[1], args[2])


def addan(inf1, inf2, newf):
    """
    Adds Tsys block from infile2 to the end of infile1 and write the result to newfile.
    """
    outfile = open(newf, 'w')
    with open(inf1, 'r') as infile:
        for line in infile:
            outfile.write(line)
    outfile.write('\n')
    with open(inf2, 'r') as infile:
        for line in infile:
            if 'TSYS' in line or 'INDEX' in line:
                outfile.write(line)
    outfile.write('/\n')
    with open(inf2, 'r') as infile:
        for line in infile:
            if ':' in line and 'TSYS' not in line and 'INDEX' not in line:
                outfile.write(line)
    outfile.write('/')
    outfile.close()


if __name__=="__main__":
    main()
