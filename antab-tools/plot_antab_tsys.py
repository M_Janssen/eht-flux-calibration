#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Plot Tsys values from ANTAB files as a function of time.
"""
import sys
import time
import numpy as np
import matplotlib.pyplot as plt
from optparse import OptionParser

def main():
    usage       = "%prog antab-file station-code [--p] [-h or --help]"
    parser      = OptionParser(usage=usage)
    parser.add_option("--p", action='store_true', help=r"Do not show plot.")
    (opts,args) = parser.parse_args()
    if len(sys.argv)<3:
        parser.print_help()
        sys.exit(1)
    plot_tsys(args[0], args[1], opts.p)


def plot_tsys(antabf, st_code, noplot=False):
    """
    Plot Tsys values from antabf as a function of time for station st_code.
    """
    f          = open(antabf, 'r')
    lines      = f.readlines()
    tsysplot   = []
    timeplot   = []
    rightblock = False
    for i, l in enumerate(lines):
        thisline       = l.strip('\n').split()
        thisline_lower = [tl.lower() for tl in thisline]
        if rightblock and any(thisline):
            try:
                _  = int(thisline[0])
                timeplot.append(antab_time_to_s(thisline[0], thisline[1]))
                tsysplot.append([float(tsys) for tsys in thisline[2:]])

            except ValueError:
                pass
        if 'tsys' in thisline_lower:
            if st_code.lower() in thisline_lower:
                rightblock = True
            elif any(tsysplot):
                rightblock = False
            else:
                pass
    if not tsysplot:
        print ('Nothing selected')
        return False
    tsysplot = np.swapaxes(tsysplot,0,1)
    timeplot = np.asarray(timeplot) - min(timeplot)
    labels   = ['RCP', 'LCP']
    for i, tsys in enumerate(tsysplot):
        plt.scatter(timeplot, tsys, label=labels[i])
    plt.ylabel('Tsys [K]')
    plt.xlabel('Time [s]')
    plt.yscale('log')
    plt.legend(loc='best')
    print('Smallest Tsys: {0}K'.format(np.min(tsysplot)))
    if not noplot:
        plt.show()
    f.close()


def antab_time_to_s(_ddd, _hhmmss, year='2016'):
    if '.' in _hhmmss:
        splitmin = _hhmmss.split('.')
        _hhmmss  = splitmin[0] + ':' + str(int(60*float('0.{0}'.format(splitmin[1]))))
    _t = '{0}-{1}-{2}'.format(year,_ddd, _hhmmss)
    return time.mktime(time.strptime(_t, '%Y-%j-%H:%M:%S'))


if __name__=="__main__":
    main()
