#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""
Print unexpected Tsys outliers.
Reasonable boundary values are hard-coded in check_antab().
"""
import sys
from optparse import OptionParser


def main():
    usage       = "%prog antabfile [-h or --help]"
    parser      = OptionParser(usage=usage)
    (opts,args) = parser.parse_args()
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)
    check_antab(args[0])


def check_antab(antab):
    """
    Prints unexpected Tsys outliers in an ANTAB file based on the boundary values
    specified with the telescope_boundaries dict below.
    """
    telescope_boundaries       = {}
    telescope_boundaries['AA'] = [0.05, 20.0]
    telescope_boundaries['LM'] = [10, 3000]
    telescope_boundaries['AZ'] = [10, 1000]
    telescope_boundaries['SP'] = [10, 1000]
    telescope_boundaries['AP'] = [10, 1000]
    telescope_boundaries['PV'] = [10, 1000]
    telescope_boundaries['SR'] = [10, 1000]
    telescope_boundaries['SM'] = [1000, 100000]
    telescope_boundaries['JC'] = [300, 30000]

    bad_lines = []
    bad_srcs  = []

    current_telescope = None
    with open(antab) as f:
        got_vals = False
        for i,line in enumerate(f):
            for ant in telescope_boundaries.keys():
                if ant in line:
                    current_telescope = ant
                    all_tsys          = {}
                    all_tsys['t']     = []
                    all_tsys['l']     = []
                    all_tsys['s']     = []
                    break
            if ':' in line:
                #store values
                these_vals = line.split()
                try:
                    _ = int(these_vals[0])
                    these_tsys      = {}
                    these_tsys['t'] = []
                    these_tsys['l'] = []
                    these_tsys['s'] = []
                    for val in these_vals[1:]:
                        try:
                            got_vals = True
                            these_tsys['t'].append(float(val))
                            these_tsys['l'].append(i+1)
                            these_tsys['s'].append(current_telescope)
                        except ValueError:
                            pass
                    all_tsys['t'].append(these_tsys['t'])
                    all_tsys['l'].append(these_tsys['l'][0])
                    all_tsys['s'].append(these_tsys['s'][0])
                except ValueError:
                    pass
            elif got_vals:
                #process values
                got_vals = False
                for t, l, s in zip(all_tsys['t'], all_tsys['l'], all_tsys['s']):
                    bad = False
                    for tt in t:
                        if tt < telescope_boundaries[current_telescope][0] or tt > telescope_boundaries[current_telescope][1]:
                            bad = True
                            break
                    if bad:
                        bad_lines.append(l)
                        bad_srcs.append(s)
    bad_lines_formatted = ''
    current_line_count  = []
    skip_next           = False
    for i, line in enumerate(bad_lines):
        if skip_next:
            skip_next = False
            continue
        try:
            next_line = bad_lines[i+1]
            if line+1 == next_line:
                current_line_count.append(line)
                current_line_count.append(next_line)
                skip_next = True
            else:
                this_src = get_src(i, current_line_count, bad_lines, bad_srcs)
                bad_lines_formatted += dump_vals(line, current_line_count, this_src)
                current_line_count  = []
        except IndexError:
            this_src = get_src(i, current_line_count, bad_lines, bad_srcs)
            bad_lines_formatted += dump_vals(line, current_line_count, this_src)
    print 'bad lines:'
    print bad_lines_formatted


def dump_vals(_i, _current_line_count, _source):
    if _current_line_count:
        return '{0}~{1} -- {2}\n'.format(str(min(_current_line_count)), str(max(_current_line_count)), str(_source))
    else:
        return '{0} -- {1}\n'.format(str(_i), str(_source))


def get_src(i, _current_line_count, _bad_lines, _bad_srcs):
    if _current_line_count:
        indx = _bad_lines.index(_current_line_count[0])
    else:
        indx = i
    return _bad_srcs[indx]


if __name__=="__main__":
    main()
