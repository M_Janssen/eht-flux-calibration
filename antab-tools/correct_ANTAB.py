#!/usr/bin/env python
#
# Copyright (C) 2019 Michael Janssen
#
# This library is free software; you can redistribute it and/or modify it
# under the terms of the GNU Library General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
# License for more details.
#
# You should have received a copy of the GNU Library General Public License
# along with this library; if not, write to the Free Software Foundation,
# Inc., 675 Massachusetts Ave, Cambridge, MA 02139, USA.
#
"""Given a station code, time range, and allowed Tsys range, replaces all invalid Tsys numbers by interpolation."""
import sys
import time
from optparse import OptionParser
from scipy.interpolate import interp1d


def main():
    usage       = "%prog antabfile stationcode t1a[ddd] t1b[hh:mm:ss] t2a[ddd] t2b[hh:mm:ss] Tmin Tmax [-h or --help]"
    parser      = OptionParser(usage=usage)
    (opts,args) = parser.parse_args()
    if len(sys.argv)<9:
        parser.print_help()
        sys.exit(1)
    smoothantab(args[0], args[1], args[2], args[3], args[4], args[5], float(args[6]), float(args[7]))


def smoothantab(antabf, st, t1a, t1b, t2a, t2b, tmin, tmax):
    """
    Given a station code, time range, and allowed Tsys range, replaces all invalid Tsys numbers by interpolation.
    """
    starttime  = get_time(t1a, t1b)
    endtime    = get_time(t2a, t2b)
    f          = open(antabf, 'r')
    lines      = f.readlines()
    newlines   = {}
    rightblock = False
    for i, l in enumerate(lines):
        thisline       = l.strip('\n').split()
        thisline_lower = [tl.lower() for tl in thisline]
        if rightblock:
            try:
                _            = int(thisline[0])
                thistime     = get_time(thisline[0], thisline[1])
                tsys_antab   = [float(tsys) for tsys in thisline[2:]]
                if thistime >= starttime and thistime <= endtime:
                    t0          = thisline[0] + ' ' + thisline[1]
                    newlines[i] = [l, (thistime, tsys_antab, t0)]
                elif thistime > endtime:
                    break
            except ValueError:
                pass
        if st.lower() in thisline_lower and 'tsys' in thisline_lower and 'index' in thisline_lower:
            rightblock = True
    if not newlines:
        print ('Nothing selected')
        return False
    goodvals_x = []
    goodvals_y = []
    bad_indx   = []
    for i in newlines.keys():
        tsys = newlines[i][1][1]
        apnd = True
        for val in tsys:
            if val < tmin or val > tmax:
                apnd = False
                bad_indx.append(i)
                break
        if apnd:
            goodvals_x.append(newlines[i][1][0])
            goodvals_y.append(tsys)
    gy        = list(map(list, zip(*goodvals_y)))
    interpt = [interp1d(goodvals_x, y, kind='linear', bounds_error=False, fill_value=(y[0],y[-1])) for y in gy]
    for i in bad_indx:
        tsys = newlines[i][1][1]
        for j, val in enumerate(tsys):
            if val < tmin or val > tmax:
                tsys[j] = interpt[j](newlines[i][1][0])
        newtsys     = [str(val) for val in tsys]
        newtsys_str = ' '.join(newtsys)
        newline     = newlines[i][1][2] + ' ' + newtsys_str + '\n'
        print ('Replacing\n  ' + newlines[i][0] + 'by\n  ' + newline)
        lines[i] = newline
    f = open(antabf, 'w')
    for l in lines:
        f.write(l)
    f.close()


def get_time(_ddd, _hhmmss):
    _t = '2016-{0}-{1}'.format(_ddd, _hhmmss)
    return time.mktime(time.strptime(_t, '%Y-%j-%H:%M:%S'))


if __name__=="__main__":
    main()
