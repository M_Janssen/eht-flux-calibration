README file for Event Horizon Telescope (EHT) flux calibration tools.  
By Michael Janssen.  
Questions, comments, and bug reports should be sent to M.Janssen@astro.ru.nl.  
  
Typcal order of operation for ANTAB generation:  
===============================================  
1) Create a primary ANTAB file for all stations but ALMA.  
2) Use concat_antab.py to concatenate all scan-based ALMA ANTAB files into a master ALMA ANTAB file.  
3) Use attach_ALMA.py to attach the master ALMA ANTAB file to the primary ANTAB file.  
4) Use correct_ANTAB.py to smooth over known spurious Tsys values.  
5) Use scale_tsys.py to adjust specific Tsys values, correcting for specific data issues.  
